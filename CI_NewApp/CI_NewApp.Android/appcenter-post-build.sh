#!/usr/bin/env bash

# Post Build Script

set -e # Exit immediately if a command exits with a non-zero status (failure)

echo "**************************************************************************************************"
echo "Post Build Script"
echo "**************************************************************************************************"

##################################################
# Start UI Tests
##################################################

# variables
appCenterLoginApiToken=$AccesstokenforLogointoAutomatedUITesting # this comes from the build environment variables
appName="gishamary88/CI_NewApp_Android"
deviceSetName="gishamary88/Set2"
testSeriesName="SmokeTests"
testToolsDirectory="/Users/vsts/.nuget/packages/xamarin.uitest/2.2.7/lib/tools"
appppath="/Users/vsts/agent/2.142.1/work/1/s/CI_NewApp/CI_NewApp.Android/obj/Release/android/bin/com.companyname.CI_NewApp.apk"
apppath="/Users/vsts/agent/2.144.0/work/1/a/build/com.companyname.CI_NewApp.apk"
builddir="$APPCENTER_SOURCE_DIRECTORY/CI_NewApp/UITest_test/bin/Release"


echo ""
echo "Start Xamarin.UITest run"
echo "   App Name: $appName"
echo " Device Set: $deviceSetName"
echo "Test Series: $testSeriesName"
echo ""

echo "> Run UI test command"

# Note: must put a space after each parameter/value pair
#echo ">appcenter test run uitest --app "gishamary88/CI_NewApp_Android" --devices "gishamary88/Set2" --app-path "$APPCENTER_OUTPUT_DIRECTORY/CI_NewApp.Android/bin/Release/com.companyname.CI_NewApp-Signed.apk" --test-series "SmokeTests" --locale "en_US" --build-dir "$APPCENTER_SOURCE_DIRECTORY/UITest_test/bin/Release" --uitest-tools-dir "$APPCENTER_SOURCE_DIRECTORY/UITest_test/bin/Release/packages/xamarin.uitest/2.2.7/tools" --token $AccesstokenforLogointoAutomatedUITesting
sleep 10s

appcenter test run uitest --app $appName --devices $deviceSetName --app-path $apppath --test-series $testSeriesName --locale "en_US" --build-dir $builddir --uitest-tools-dir $testToolsDirectory --token $appCenterLoginApiToken

sleep 10s
echo ""
echo "**************************************************************************************************"
echo "Post Build Script complete"
echo "**************************************************************************************************"
