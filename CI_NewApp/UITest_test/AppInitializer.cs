﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UITest_test
{
    public class AppInitializer
    {
        public static IApp StartApp(Platform platform)
        {
            if (platform == Platform.Android)
            {
                return ConfigureApp
                    .Android
                    .ApkFile("C:\\Users\\gisha\\source\\repos\\CI_NewApp\\CI_NewApp\\CI_NewApp.Android\\bin\\Release\\com.companyname.CI_NewApp-Signed.apk")
                    .StartApp();

            }

            return ConfigureApp.iOS.StartApp();
        }
    }
}